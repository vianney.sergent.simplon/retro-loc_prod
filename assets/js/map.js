var mymap = L.map('mapid').setView([45.590325, 5.920590], 13);
/*Pointeur MovieLook*/
var marker = L.marker([45.590325, 5.920590]).addTo(mymap);
marker.bindPopup("Ici RetroLoc!").openPopup();
/*End MovieLook*/
/*Pointeur Dominos*/
var marker = L.marker([45.570898, 5.93721]).addTo(mymap);
marker.bindPopup("Ici Dominos!").openPopup();
/*End Dominos*/
/*Pointeur Mondial Pizza */
var marker = L.marker([45.573373, 5.910974]).addTo(mymap);
marker.bindPopup("Ici Mondial Pizza!").openPopup();
/*End Mondial Pizza*/
/*Pointeur Basilic and Co*/
var marker = L.marker([45.566098, 5.917317]).addTo(mymap);
marker.bindPopup("Ici Basilic and Co!").openPopup();
/*End Basilic and Co*/

var circle = L.circle([45.590325, 5.920590], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 2500
}).addTo(mymap);


L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,}) 
    .addTo(mymap);





