<?php

namespace App\Entity;

use App\Repository\StatutVinyleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StatutVinyleRepository::class)
 */
class StatutVinyle
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=Vinyle::class, mappedBy="Statut_Vinyle")
     */
    private $Statut_Vinyle;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $statut;

  

    public function __construct()
    {
        $this->Statut_Vinyle = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Vinyle[]
     */
    public function getStatutVinyle(): Collection
    {
        return $this->Statut_Vinyle;
    }

    public function addStatutVinyle(Vinyle $statutVinyle): self
    {
        if (!$this->Statut_Vinyle->contains($statutVinyle)) {
            $this->Statut_Vinyle[] = $statutVinyle;
            $statutVinyle->setStatutVinyle($this);
        }

        return $this;
    }

    public function removeStatutVinyle(Vinyle $statutVinyle): self
    {
        if ($this->Statut_Vinyle->contains($statutVinyle)) {
            $this->Statut_Vinyle->removeElement($statutVinyle);
            // set the owning side to null (unless already changed)
            if ($statutVinyle->getStatutVinyle() === $this) {
                $statutVinyle->setStatutVinyle(null);
            }
        }

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function __toString()
    {
        return $this->statut;
    }

    
}
