<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\BorrowingRepository;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=BorrowingRepository::class)
 */
class Borrowing
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
   
    /**
     * @ORM\Column(type="datetime")
     */
    private $start_date;

    /**
     * @ORM\Column(type="datetime")
     */
    private $end_date;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=StatutLocation::class, inversedBy="borrowings")
     */
    private $Statut_borrowing;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="Borrowing")
     */
    private $purchase;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $borrower;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ref;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $refsent;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $refback;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $borrowingstatus; 
        

    public function getId(): ?int
    {
        return $this->id;
    }  

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->start_date;
    }

    public function setStartDate(\DateTimeInterface $start_date): self
    {
        $this->start_date = $start_date;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->end_date;
    }

    public function setEndDate(\DateTimeInterface $end_date): self
    {
        $this->end_date = $end_date;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getStatutBorrowing(): ?StatutLocation
    {
        return $this->Statut_borrowing;
    }

    public function setStatutBorrowing(?StatutLocation $Statut_borrowing): self
    {
        $this->Statut_borrowing = $Statut_borrowing;

        return $this;
    }

    public function getPurchase(): ?User
    {
        return $this->purchase;
    }

    public function setPurchase(?User $purchase): self
    {
        $this->purchase = $purchase;

        return $this;
    }

    public function getBorrower(): ?string
    {
        return $this->borrower;
    }

    public function setBorrower(string $borrower): self
    {
        $this->borrower = $borrower;

        return $this;
    }

    public function getRef(): ?string
    {
        return $this->ref;
    }

    public function setRef(string $ref): self
    {
        $this->ref = $ref;

        return $this;
    }

    public function getRefsent(): ?string
    {
        return $this->refsent;
    }

    public function setRefsent(string $refsent): self
    {
        $this->refsent = $refsent;

        return $this;
    }

    public function getRefback(): ?string
    {
        return $this->refback;
    }

    public function setRefback(string $refback): self
    {
        $this->refback = $refback;

        return $this;
    }

    public function getBorrowingstatus(): ?string
    {
        return $this->borrowingstatus;
    }

    public function setBorrowingstatus(string $borrowingstatus): self
    {
        $this->borrowingstatus = $borrowingstatus;

        return $this;
    }
  

  
}   

   
