<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 *  @UniqueEntity(
 * fields = {"email"},
 * message ="l'email que vous avez indiqué est deja utilisé !")
 *  @Vich\Uploadable
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")     
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

     /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @ORM\Column(type="string", length=255)
     *  @Assert\Length(min="8", minMessage="Votre mot de passe doit faire minimum 8 caractère") 
     */
    private $password;

     /**
    * @Assert\EqualTo(propertyPath="password", message="Vous n'avez pas tapé le même mot de passe")
     */

    public $Confirm_Password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $postal_code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @ORM\OneToMany(targetEntity=Borrowing::class, mappedBy="Borrowing")
     */
    private $Borrowing;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Genres;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $subscription;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $subscriptionstatus;

  

   

  

   

    public function __construct()
    {
        $this->Borrowing = new ArrayCollection();
    }

    // ... other fields

   
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }
    public function eraseCredentials() {}

    public function getSalt() {}

    public function getRoles(): ?array
    {
        $roles = $this->roles;  
        //guarantee every user at least has ROLE_USER      
        $roles[] = 'ROLE_USER';
        
        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postal_code;
    }

    public function setPostalCode(string $postal_code): self
    {
        $this->postal_code = $postal_code;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return Collection|Borrowing[]
     */
    public function getBorrowing(): Collection
    {
        return $this->Borrowing;
    }

    public function addBorrowing(Borrowing $borrowing): self
    {
        if (!$this->Borrowing->contains($borrowing)) {
            $this->Borrowing[] = $borrowing;
            $borrowing->setPurchase($this);
        }

        return $this;
    }

    public function removeBorrowing(Borrowing $borrowing): self
    {
        if ($this->Borrowing->contains($borrowing)) {
            $this->Borrowing->removeElement($borrowing);
            // set the owning side to null (unless already changed)
            if ($borrowing->getPurchase() === $this) {
                $borrowing->setPurchase(null);
            }
        }

        return $this;
    }

    public function getGenres(): ?string
    {
        return $this->Genres;
    }

    public function setGenres(string $Genres): self
    {
        $this->Genres = $Genres;

        return $this;
    }

    public function getSubscription(): ?string
    {
        return $this->subscription;
    }

    public function setSubscription(string $subscription): self
    {
        $this->subscription = $subscription;

        return $this;
    }

    public function getSubscriptionstatus(): ?string
    {
        return $this->subscriptionstatus;
    }

    public function setSubscriptionstatus(string $subscriptionstatus): self
    {
        $this->subscriptionstatus = $subscriptionstatus;

        return $this;
    }
    public function __toString()
    {
        return $this->username;
    }

   

   
   

  
}
