<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\VinyleRepository;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\Collections\ArrayCollection;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Vich\UploaderBundle\Entity\File as EmbeddedFile;

/**
 * @ORM\Entity(repositoryClass=VinyleRepository::class) 
 * @ORM\Table(name="Vinyle", indexes={@ORM\Index(columns={"title"}, flags={"fulltext"})})
 * @Vich\Uploadable
 */
class Vinyle
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;       

    /**
     * @ORM\Column(type="boolean")
     */
    private $avaibility;

    /**
     * @ORM\ManyToOne(targetEntity=Genres::class, inversedBy="vinyles")
     */
    private $vinyle_genre;

    /**
     * @ORM\ManyToOne(targetEntity=StatutVinyle::class, inversedBy="Statut_Vinyle")
     */
    private $Statut_Vinyle;

    /**
     * @ORM\ManyToOne(targetEntity=Interpreter::class, inversedBy="Interpreter")
     */
    private $interpreter;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $borrower;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ref;   

      /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="Pochettes_Images", fileNameProperty="image.name", size="image.size", mimeType="image.mimeType", originalName="image.originalName", dimensions="image.dimensions")
     * 
     * @var File|null
     */
    private $imageFile;
     /**
     * @ORM\Embedded(class="Vich\UploaderBundle\Entity\File")
     *
     * @var EmbeddedFile
     */
    private $image;

    /**
     * @ORM\Column(type="text")
     */
    private $audio_tracks;

   

    
  
    public function __construct()
    {
        $this->genres = new ArrayCollection();
        $this->image = new EmbeddedFile();      
    }
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;      
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }
   
    
    public function setImage(EmbeddedFile $image): void
    {
        $this->image = $image;
    }

    public function getImage(): ?EmbeddedFile
    {
        return $this->image;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }   
     

    public function getAvaibility(): ?bool
    {
        return $this->avaibility;
    }

    public function setAvaibility(bool $avaibility): self
    {
        $this->avaibility = $avaibility;

        return $this;
    }

    public function getVinyleGenre(): ?Genres
    {
        return $this->vinyle_genre;
    }

    public function setVinyleGenre(?Genres $vinyle_genre): self
    {
        $this->vinyle_genre = $vinyle_genre;

        return $this;
    }   

    public function getStatutVinyle(): ?StatutVinyle
    {
        return $this->Statut_Vinyle;
    }

    public function setStatutVinyle(?StatutVinyle $Statut_Vinyle): self
    {
        $this->Statut_Vinyle = $Statut_Vinyle;

        return $this;
    }

    public function getInterpreter(): ?Interpreter
    {
        return $this->interpreter;
    }

    public function setInterpreter(?Interpreter $interpreter): self
    {
        $this->interpreter = $interpreter;

        return $this;
    }

    public function getBorrower(): ?string
    {
        return $this->borrower;
    }

    public function setBorrower(string $borrower): self
    {
        $this->borrower = $borrower;

        return $this;
    }

    public function getRef(): ?string
    {
        return $this->ref;
    }

    public function setRef(string $ref): self
    {
        $this->ref = $ref;

        return $this;
    }

    public function getAudioTracks(): ?string
    {
        return $this->audio_tracks;
    }

    public function setAudioTracks(string $audio_tracks): self
    {
        $this->audio_tracks = $audio_tracks;

        return $this;
    }

  
  

}


