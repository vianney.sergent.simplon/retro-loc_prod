<?php

namespace App\Entity;

use App\Repository\InterpreterRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InterpreterRepository::class)
 */
class Interpreter
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

   

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Vinyle::class, mappedBy="interpreter")
     */
    private $Interpreter;

    public function __construct()
    {
        $this->interpreter = new ArrayCollection();
        $this->Interpreter = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    
    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Vinyle[]
     */
    public function getInterpreter(): Collection
    {
        return $this->Interpreter;
    }

    public function addInterpreter(Vinyle $interpreter): self
    {
        if (!$this->Interpreter->contains($interpreter)) {
            $this->Interpreter[] = $interpreter;
            $interpreter->setInterpreter($this);
        }

        return $this;
    }

    public function removeInterpreter(Vinyle $interpreter): self
    {
        if ($this->Interpreter->contains($interpreter)) {
            $this->Interpreter->removeElement($interpreter);
            // set the owning side to null (unless already changed)
            if ($interpreter->getInterpreter() === $this) {
                $interpreter->setInterpreter(null);
            }
        }

        return $this;
    }
    public function __toString()
    {
        return $this->name;
    }
}
