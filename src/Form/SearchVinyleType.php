<?php

namespace App\Form;

use App\Entity\Genres;
use App\Entity\Interpreter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class SearchVinyleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('mots', SearchType::class,[
                'label'=> false,
                'attr' =>[
                    'class'=>'form-control',
                    'placeholder'=>'Entrez un ou plusieurs mots du titre '
                ],
                'required'=>false
            ])
            ->add('Rechercher', SubmitType::class,[
                'attr' =>[
                    'class'=>'btn btn-success',
            ]
        ])
        ->add('interpreter', EntityType::class, [
            'class' => Interpreter::class,
            'label' => false,            
            'attr' => [
                'class' => 'form-control'      
            ],
            'placeholder' => 'Interprètes',
            'required' => false
        ])
        ->add('vinyle_genre', EntityType::class, [
            'class' => Genres::class,
            'label' => false,
            'attr' => [
                'class' => 'form-control',
            ],
            'placeholder' => 'Genres',
            'required' => false
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
