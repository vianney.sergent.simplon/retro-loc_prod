<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('genres',ChoiceType::class,array(
            'choices'  => array(
                'Monsieur' => 'Monsieur',
                'Madame' => 'Madame',                    
            ),
            'expanded' => true,
            'multiple' => false
        )) 
            ->add('address')
            ->add('phone')
            ->add('city')
            ->add('postal_code')
            ->add('firstname')
            ->add('lastname')
            ->add('email')
            ->add('username')
            ->add('password',PasswordType::class)
            ->add('Confirm_Password',PasswordType::class)
            ->add('subscription',ChoiceType::class,array(
                'choices'  => array(
                    'Annuel plein (20€)' => 'Annuel Réduit',
                    'Annuel Réduit(10€)'=>'Annuel Réduit',
                    'Mensuel plein (5€)' => 'Mensuel',
                    'Mensuel Réduit(2€)'=>'Mensuel Réduit'                    
                ),
                'expanded' => true,
                'multiple' => false
            ))
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
