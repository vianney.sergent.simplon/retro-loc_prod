<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('genres',ChoiceType::class,array(
            'choices'  => array(
                'Monsieur' => 'Monsieur',
                'Madame' => 'Madame',                    
            ),
            'expanded' => true,
            'multiple' => false
        )) 
            ->add('username')
            ->add('email')   
            ->add('address')
            ->add('phone')
            ->add('city')
            ->add('postal_code')
            ->add('firstname')
            ->add('lastname') 
            ->add('subscription',ChoiceType::class,array(
                'choices'  => array(
                    'Mensuel (3€)' => 'Mensuel',
                    'Annuel (15€)' => 'Annuel',                    
                ),
                'expanded' => true,
                'multiple' => false
            ))  
            ->add('subscriptionstatus',ChoiceType::class,array(
                'choices'  => array(
                    'En Attente' => 'En Attente',
                    'Validé' => 'Validé', 
                    'Clôturé'  => 'Clôturé'                 
                ),
                'expanded' => true,
                'multiple' => false
            ))              
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
