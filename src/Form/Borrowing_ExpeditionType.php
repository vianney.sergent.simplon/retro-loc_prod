<?php

namespace App\Form;


use App\Entity\Borrowing;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class Borrowing_ExpeditionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder           
                    
            ->add('borrowingstatus',ChoiceType::class,array(
                'choices'  => array(
                    'Soumise' => 'Soumise',
                    'Validée' => 'Validée',
                    'Expédiée' => 'Expédiée',
                    'Réceptionnée' => 'Réceptionnée',
                    'Retournée' => 'Retournée',
                    'Cloturée' => 'Cloturée'
                ),
                'expanded' => true,
                'multiple' => false
            ))              
            ->add('refsent')
           
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Borrowing::class,
            
          
           
        ]);
    }
}  
