<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201007083855 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE vinyle DROP FOREIGN KEY FK_8CD238D035246A4D');
        $this->addSql('ALTER TABLE vinyle DROP FOREIGN KEY FK_8CD238D07B575941');
        $this->addSql('ALTER TABLE vinyle DROP FOREIGN KEY FK_8CD238D0AD59FFB1');
        $this->addSql('CREATE FULLTEXT INDEX IDX_8B7E3DE62B36786B ON vinyle (title)');
        $this->addSql('DROP INDEX idx_8cd238d07b575941 ON vinyle');
        $this->addSql('CREATE INDEX IDX_8B7E3DE67B575941 ON vinyle (vinyle_genre_id)');
        $this->addSql('DROP INDEX idx_8cd238d035246a4d ON vinyle');
        $this->addSql('CREATE INDEX IDX_8B7E3DE635246A4D ON vinyle (statut_vinyle_id)');
        $this->addSql('DROP INDEX idx_8cd238d0ad59ffb1 ON vinyle');
        $this->addSql('CREATE INDEX IDX_8B7E3DE6AD59FFB1 ON vinyle (interpreter_id)');
        $this->addSql('ALTER TABLE vinyle ADD CONSTRAINT FK_8CD238D035246A4D FOREIGN KEY (statut_vinyle_id) REFERENCES statut_vinyle (id)');
        $this->addSql('ALTER TABLE vinyle ADD CONSTRAINT FK_8CD238D07B575941 FOREIGN KEY (vinyle_genre_id) REFERENCES genres (id)');
        $this->addSql('ALTER TABLE vinyle ADD CONSTRAINT FK_8CD238D0AD59FFB1 FOREIGN KEY (interpreter_id) REFERENCES interpreter (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX IDX_8B7E3DE62B36786B ON Vinyle');
        $this->addSql('ALTER TABLE Vinyle DROP FOREIGN KEY FK_8B7E3DE67B575941');
        $this->addSql('ALTER TABLE Vinyle DROP FOREIGN KEY FK_8B7E3DE635246A4D');
        $this->addSql('ALTER TABLE Vinyle DROP FOREIGN KEY FK_8B7E3DE6AD59FFB1');
        $this->addSql('DROP INDEX idx_8b7e3de6ad59ffb1 ON Vinyle');
        $this->addSql('CREATE INDEX IDX_8CD238D0AD59FFB1 ON Vinyle (interpreter_id)');
        $this->addSql('DROP INDEX idx_8b7e3de67b575941 ON Vinyle');
        $this->addSql('CREATE INDEX IDX_8CD238D07B575941 ON Vinyle (vinyle_genre_id)');
        $this->addSql('DROP INDEX idx_8b7e3de635246a4d ON Vinyle');
        $this->addSql('CREATE INDEX IDX_8CD238D035246A4D ON Vinyle (statut_vinyle_id)');
        $this->addSql('ALTER TABLE Vinyle ADD CONSTRAINT FK_8B7E3DE67B575941 FOREIGN KEY (vinyle_genre_id) REFERENCES genres (id)');
        $this->addSql('ALTER TABLE Vinyle ADD CONSTRAINT FK_8B7E3DE635246A4D FOREIGN KEY (statut_vinyle_id) REFERENCES statut_vinyle (id)');
        $this->addSql('ALTER TABLE Vinyle ADD CONSTRAINT FK_8B7E3DE6AD59FFB1 FOREIGN KEY (interpreter_id) REFERENCES interpreter (id)');
    }
}
