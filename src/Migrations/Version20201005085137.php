<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201005085137 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE contact2 (id INT AUTO_INCREMENT NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, message VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE borrowing ADD purchase_id INT DEFAULT NULL, ADD borrower VARCHAR(255) NOT NULL, ADD ref VARCHAR(255) NOT NULL, ADD refsent VARCHAR(255) NOT NULL, ADD refback VARCHAR(255) NOT NULL, ADD borrowingstatus VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE borrowing ADD CONSTRAINT FK_226E5897558FBEB9 FOREIGN KEY (purchase_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_226E5897558FBEB9 ON borrowing (purchase_id)');
        $this->addSql('ALTER TABLE user ADD genres VARCHAR(255) NOT NULL, ADD subscription VARCHAR(255) NOT NULL, ADD subscriptionstatus VARCHAR(255) DEFAULT NULL, CHANGE roles roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\'');
        $this->addSql('ALTER TABLE vinyle DROP FOREIGN KEY FK_8CD238D07FE45227');
        $this->addSql('DROP INDEX IDX_8CD238D07FE45227 ON vinyle');
        $this->addSql('ALTER TABLE vinyle ADD borrower VARCHAR(255) NOT NULL, ADD ref VARCHAR(255) NOT NULL, ADD image_name VARCHAR(255) DEFAULT NULL, ADD image_original_name VARCHAR(255) DEFAULT NULL, ADD image_mime_type VARCHAR(255) DEFAULT NULL, ADD image_dimensions LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', CHANGE aviability avaibility TINYINT(1) NOT NULL, CHANGE statut_location_id image_size INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE contact2');
        $this->addSql('ALTER TABLE borrowing DROP FOREIGN KEY FK_226E5897558FBEB9');
        $this->addSql('DROP INDEX IDX_226E5897558FBEB9 ON borrowing');
        $this->addSql('ALTER TABLE borrowing DROP purchase_id, DROP borrower, DROP ref, DROP refsent, DROP refback, DROP borrowingstatus');
        $this->addSql('ALTER TABLE user DROP genres, DROP subscription, DROP subscriptionstatus, CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
        $this->addSql('ALTER TABLE vinyle DROP borrower, DROP ref, DROP image_name, DROP image_original_name, DROP image_mime_type, DROP image_dimensions, CHANGE image_size statut_location_id INT DEFAULT NULL, CHANGE avaibility aviability TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE vinyle ADD CONSTRAINT FK_8CD238D07FE45227 FOREIGN KEY (statut_location_id) REFERENCES statut_location (id)');
        $this->addSql('CREATE INDEX IDX_8CD238D07FE45227 ON vinyle (statut_location_id)');
    }
}
