<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201004204737 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE borrowing (id INT AUTO_INCREMENT NOT NULL, statut_borrowing_id INT DEFAULT NULL, purchase_id INT DEFAULT NULL, start_date DATETIME NOT NULL, end_date DATETIME NOT NULL, created_at DATETIME NOT NULL, borrower VARCHAR(255) NOT NULL, ref VARCHAR(255) NOT NULL, refsent VARCHAR(255) NOT NULL, refback VARCHAR(255) NOT NULL, borrowingstatus VARCHAR(255) NOT NULL, INDEX IDX_226E5897E906425B (statut_borrowing_id), INDEX IDX_226E5897558FBEB9 (purchase_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contact2 (id INT AUTO_INCREMENT NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, message VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE genres (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE interpreter (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE statut_location (id INT AUTO_INCREMENT NOT NULL, statut VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE statut_vinyle (id INT AUTO_INCREMENT NOT NULL, statut VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, address VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, postal_code VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, genres VARCHAR(255) NOT NULL, subscription VARCHAR(255) NOT NULL, subscriptionstatus VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vinyle (id INT AUTO_INCREMENT NOT NULL, vinyle_genre_id INT DEFAULT NULL, statut_vinyle_id INT DEFAULT NULL, interpreter_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, avaibility TINYINT(1) NOT NULL, borrower VARCHAR(255) NOT NULL, ref VARCHAR(255) NOT NULL, INDEX IDX_8CD238D07B575941 (vinyle_genre_id), INDEX IDX_8CD238D035246A4D (statut_vinyle_id), INDEX IDX_8CD238D0AD59FFB1 (interpreter_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE borrowing ADD CONSTRAINT FK_226E5897E906425B FOREIGN KEY (statut_borrowing_id) REFERENCES statut_location (id)');
        $this->addSql('ALTER TABLE borrowing ADD CONSTRAINT FK_226E5897558FBEB9 FOREIGN KEY (purchase_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE vinyle ADD CONSTRAINT FK_8CD238D07B575941 FOREIGN KEY (vinyle_genre_id) REFERENCES genres (id)');
        $this->addSql('ALTER TABLE vinyle ADD CONSTRAINT FK_8CD238D035246A4D FOREIGN KEY (statut_vinyle_id) REFERENCES statut_vinyle (id)');
        $this->addSql('ALTER TABLE vinyle ADD CONSTRAINT FK_8CD238D0AD59FFB1 FOREIGN KEY (interpreter_id) REFERENCES interpreter (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE vinyle DROP FOREIGN KEY FK_8CD238D07B575941');
        $this->addSql('ALTER TABLE vinyle DROP FOREIGN KEY FK_8CD238D0AD59FFB1');
        $this->addSql('ALTER TABLE borrowing DROP FOREIGN KEY FK_226E5897E906425B');
        $this->addSql('ALTER TABLE vinyle DROP FOREIGN KEY FK_8CD238D035246A4D');
        $this->addSql('ALTER TABLE borrowing DROP FOREIGN KEY FK_226E5897558FBEB9');
        $this->addSql('DROP TABLE borrowing');
        $this->addSql('DROP TABLE contact2');
        $this->addSql('DROP TABLE genres');
        $this->addSql('DROP TABLE interpreter');
        $this->addSql('DROP TABLE statut_location');
        $this->addSql('DROP TABLE statut_vinyle');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE vinyle');
    }
}
