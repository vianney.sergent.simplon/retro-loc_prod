<?php

namespace App\Repository;

use App\Entity\StatutVinyle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StatutVinyle|null find($id, $lockMode = null, $lockVersion = null)
 * @method StatutVinyle|null findOneBy(array $criteria, array $orderBy = null)
 * @method StatutVinyle[]    findAll()
 * @method StatutVinyle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatutVinyleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StatutVinyle::class);
    }

    // /**
    //  * @return StatutVinyle[] Returns an array of StatutVinyle objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StatutVinyle
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
