<?php

namespace App\Repository;

use App\Entity\Vinyle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Vinyle|null find($id, $lockMode = null, $lockVersion = null)
 * @method Vinyle|null findOneBy(array $criteria, array $orderBy = null)
 * @method Vinyle[]    findAll()
 * @method Vinyle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VinyleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Vinyle::class);
    }

    // /**
    //  * @return Vinyle[] Returns an array of Vinyle objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Vinyle
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function populate($ref,$borrower)
    {   $qb = $this->createQueryBuilder('v');   
       
                $qb->update()
 	            ->set('v.ref', ':ref')
	            ->where('v.borrower = :borrower')
	            ->setParameter('borrower', $borrower )
	            ->setParameter('ref', $ref)
	            ->getQuery()
                ->execute();
    }

    public function findByBorrower($borrower)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.borrower = :borrower')
            ->setParameter('borrower', $borrower)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    public function findByInterpreter($interpreter)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.interpreter = :interpreter')
            ->setParameter('intepreter', $interpreter)
            ->orderBy('v.id', 'ASC')            
            ->getQuery()
            ->getResult()
        ;
    }
    public function clear_borrowing($ref)
    {  
        $qb = $this->createQueryBuilder('v');   
        $qb->update()
 	        ->set('v.borrower', ':borrower')
	        ->set('v.avaibility', ':avaibility')	           
		    ->where('v.ref = :ref')
	        ->setParameter('borrower', '' )
	        ->setParameter('avaibility', true )
	        ->setParameter('ref',$ref)
	        ->getQuery()
            ->execute();
    }

    public function findbygenre($vinyle_genre)
    {    
        return $this->createQueryBuilder('v')
            ->andWhere('v.vinyle_genre = :vinyle_genre')
            ->setParameter('intepreter', $vinyle_genre)            
            ->getQuery()
            ->execute();        
    }

    /**
     * Recherche les vinyles en fonction du formulaire
     */

    public function search($mots = null, $interpreter, $vinyle_genre){
        $query = $this->createQueryBuilder('v');      
        if($mots != null){
            $query->andWhere('MATCH_AGAINST(v.title) AGAINST (:mots boolean)>0')
                ->setParameter('mots', $mots);
        }
        if($interpreter != null){
            $query->leftJoin('v.interpreter', 'i');
            $query->andWhere('i.id = :id')
                ->setParameter('id', $interpreter);
        }
        if($vinyle_genre != null){
            $query->leftJoin('v.vinyle_genre', 'g');
            $query->andWhere('g.id = :id')
                ->setParameter('id', $vinyle_genre);
        }

	    return $query->getQuery()->getResult();
	}

}
