<?php

namespace App\Repository;

use App\Entity\Interpreter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Interpreter|null find($id, $lockMode = null, $lockVersion = null)
 * @method Interpreter|null findOneBy(array $criteria, array $orderBy = null)
 * @method Interpreter[]    findAll()
 * @method Interpreter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InterpreterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Interpreter::class);
    }

    // /**
    //  * @return Interpreter[] Returns an array of Interpreter objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Interpreter
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
