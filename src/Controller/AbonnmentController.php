<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AbonnmentController extends AbstractController
{
    
    /**
     * @Route("/abonnement", name="abonnment")
     */

    public function index()
    {
        return $this->render('Common/abonnment/abonnement_index.html.twig');        
        
    }

    /**
     * @Route("/abonnment/annuel", name="annuel")
     */
    
    public function annuel()
    {
        return $this->render('Common/abonnment/abonnement_annuel_individuel.html.twig');
    }    
    /**
     * @Route("/abonnment/mensuel", name="mensuel" )
     */
    public function mensuel()
    {
        return $this->render('Common/abonnment/abonnement_mensuel_individuel.html.twig');
    }
}
