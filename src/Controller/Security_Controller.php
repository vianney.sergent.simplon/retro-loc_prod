<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class Security_Controller extends AbstractController
{
    /**
     * @Route("/inscrisption", name="security_registration")
     */
    public function registration(Request $request,EntityManagerInterface $manager,UserPasswordEncoderInterface $encoder ){
        $user = new User();
        $form = $this->createForm(RegistrationType::class,$user);
        

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $hash = $encoder->encodePassword($user,$form->get('password')->getData());
            $user->setPassword($hash);
            $user->setUsername($form->get('username')->getData());
            $user->setEmail($form->get('email')->getData());
            $user->setAddress($form->get('address')->getData());
            $user->setPhone($form->get('phone')->getData());
            $user->setCity($form->get('city')->getData());
            $user->setPostalCode($form->get('postal_code')->getData());
            $user->setFirstname($form->get('firstname')->getData());            
            $user->setLastname($form->get('lastname')->getData());           
            $manager->persist($user);
            $manager->flush();    
        
            return $this->redirectToRoute('security_login');
        }

        return $this->render('security/registration.html.twig',[
            'form'=> $form->createView()      
        ]); 
    }

    /**
     * @Route("/connexion", name="security_login")
     */

    public function login(){
        $this->addFlash('message', 'Votre inscription a bien été pris en compte.');
        $this->addFlash('security', 'Votre mot de passe ou email est incorrecte.');          
        return $this->render('security/login.html.twig');
    }

    /**
     * @Route("/deconnexion", name="security_logout")
     */

    public function logout(){}
}
