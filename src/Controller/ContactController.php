<?php

namespace App\Controller;


use App\Form\ContactType;
use App\Form\Contact2Type;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact")
     */
    
    public function index(Request $request)
    {
            $form=$this->createForm(Contact2Type::class);           
            $form->handleRequest($request);

            if($form->isSubmitted()&& $form->isValid){               
                $this->addFlash('success', 'Votre email a bien été envoyé');
                return $this->redirectToRoute('home');
            };
            return $this->render('Common/contact/index.html.twig', [
            'controller_name' => 'ContactController',
            'form'=>$form->createView()
            
        ]);
    }
}
