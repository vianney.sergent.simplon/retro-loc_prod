<?php

namespace App\Controller;

use DateTime;
use App\Entity\User;
use App\Entity\Vinyle;
use App\Entity\Borrowing;
use App\Form\PersoCustType;
use App\Form\SearchVinyleType;
use App\Form\Borrowing_ReturnType;
use App\Repository\UserRepository;
use App\Repository\VinyleRepository;
use App\Repository\BorrowingRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;




/**
   * @Route("/customer")
   */

class CustomerController extends AbstractController
{
    /*Index Customer*/

    /**
     * @Route("/", name="dashboard_index_customer")
     */

    public function index_dashboard_customer(UserRepository $userRepository){
        return $this->render('Customer/dashboard.index.html.twig',[
            'users' => $userRepository->findAll(),
        ]);
    }

    /**
     * @Route("/catalogue", name="catalogue_resa")
     */

    public function index(Request $request,VinyleRepository $vinyleRepository, PaginatorInterface $paginator)
    {
        $donnees = $this->getDoctrine()->getRepository(Vinyle::class)->findAll();
        $vinyle = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos articles)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            5 // Nombre de résultats par page
        );

        $vinyles=[];
        $form = $this->createForm(SearchVinyleType::class);

        $search=$form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            // ici on recherche les vinyles correspondant aux mots clés
            $vinyles= $vinyleRepository->search(
                $search->get('mots')->getData(),
                $search->get('interpreter')->getData(),
                $search->get('vinyle_genre')->getData());

        }
        return $this->render('Customer/Catalogue/Reservation.html.twig', [
            'vinyles' => $vinyles,
            'form'=>$form->createView(),
            'vinyle'=>$vinyle

        ]);
    }


    /**
     * @Route("/catalogue/{id}", name="catalogue_show", methods={"GET"})
     */

    public function show_catalogue(Vinyle $vinyle): Response
    {
        return $this->render('Customer/Catalogue/show.html.twig', [
            'vinyle' => $vinyle,
        ]);
    }

    /**
     * @Route("/{id}/{user}/select", name="vinyle_selection", methods={"GET","POST"})
     */
    public function vinyle_selection($id,$user)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $vinyle = $entityManager->getRepository(Vinyle::class)->find($id);
        if (!$vinyle) {
            throw $this->createNotFoundException(
                'No vinyle found for id '.$id
        );
        }

        $vinyle->setAvaibility(false);
        if($this->getUser())
        {
            $username = $this->getUser()->getUsername();
            $vinyle->setBorrower($username);
        }
        else{
            return $this->redirectToRoute('security_login');
        }

        $entityManager->flush();
        return $this->redirectToRoute('basket_show', [
        'id' => $vinyle->getId()
    ]);
}

    /**
     * @Route("/{id}/deselect", name="vinyle_deselection", methods={"GET","POST"})
     */

    public function vinyle_deselection($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $vinyle = $entityManager->getRepository(Vinyle::class)->find($id);
        if (!$vinyle) {
            throw $this->createNotFoundException(
                'No vinyle found for id '.$id
        );
        }

        $vinyle->setAvaibility(true);
        $vinyle->setBorrower("");
        $entityManager->flush();
        return $this->redirectToRoute('catalogue_resa', [
        'id' => $vinyle->getId()
    ]);
    }

    /**
     * @Route("/basket/show", name="basket_show", methods={"GET"})
     */

    public function show_basket(VinyleRepository $vinyleRepository): Response
    {
        if($this->getUser())
        {
        $username = $this->getUser()->getUsername();
        }
        return $this->render('Customer/basket/index.html.twig', [
            'vinyles' => $vinyleRepository->findByBorrower($username),
        ]);
    }

    /**
     * @Route("/basket/validate", name="basket_validate", methods={"GET","POST"})
     */

    public function validate_basket(VinyleRepository $vinyleRepository)
    {
        if($this->getUser())
        {
            $username = $this->getUser()->getUsername();
        }
        $currentdate=new \DateTime();
        $ref=$currentdate->format('YmdHi');
        return $this->creation_borrowing($username,$ref,$vinyleRepository);

    }

    /**
     * @Route("/{ref}/{borrower}/generate", name="borrowing_creation", methods={"GET","POST"})
     */

    public function creation_borrowing($borrower ,$ref,VinyleRepository $vinyleRepository )
    {
        $em = $this->getDoctrine()->getManager();
        $borrowing = new Borrowing();
        $username = $this->getUser()->getUsername();
        $borrowing->setBorrower($username);
        $borrowing->setRef($ref);
        $borrowing->setStartDate(new \DateTime('now'));
        $borrowing->setEndDate(new DateTime('+1 month'));
        $borrowing->setCreatedAt(new \DateTime('now'));
        $borrowing->setRefsent('none');
        $borrowing->setRefback('none');
        $borrowing->setBorrowingstatus('Soumise');

        $em->persist($borrowing);
        $em->flush();
        return $this->assign($ref,$username, $vinyleRepository);

    }

    /**
     * @Route("/{ref}/{borrower}/assign",name="assign", methods={"GET","POST"} )
     */

    public function assign($ref, $borrower,VinyleRepository $vinyleRepository)
    {
        $result= $vinyleRepository->populate($ref,$borrower);
        return $this->redirectToRoute('borrowing_display');
    }

    /**
     * @Route("/{user}/perso", name="user_perso", methods={"GET","POST"})
     */

    public function perso_user(UserRepository $userRepository)
    {

        if($this->getUser())
        {
            $username = $this->getUser();
            $user = $userRepository->find($username);
        }

        return $this->redirectToRoute('user_edit_customer', array('user' => $user));
    }

    /**
     * @Route("/perso/index", name="perso_index", methods={"GET"})
     */

    public function index_perso(UserRepository $userRepository): Response
    {

        if($this->getUser())
        {
            $userid = $this->getUser()->getId();
        }
        return $this->render('Customer/perso/index.html.twig', ['users' => $userRepository->findByUserId($userid),
        ]);

    }

    /**
     * @Route("/perso/{id}/edit", name="perso_edit_user", methods={"GET","POST"})
     */

    public function perso_edit(Request $request,user $user): Response
    {

        $form = $this->createForm(PersoCustType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('perso_index');
        }
        return $this->render('Customer/perso/edit.html.twig', ['user' => $user,'form' => $form->createView(),]);
    }

    /**
     * @Route("/borrowing/display", name="borrowing_display", methods={"GET"})
     */

    public function display_borrowing(BorrowingRepository $borrowingRepository): Response
    {
        if($this->getUser())
        {
        $username = $this->getUser()->getUsername();
        }
        return $this->render('Customer/borrowing/index.html.twig', [
            'borrowings' => $borrowingRepository->BorrowingByBorrower($username),
        ]);
    }

    /**
     * @Route("borrowing/{id}/reception", name="borrowing_reception", methods={"GET","POST"})
     */

    public function reception_borrowing($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $borrowing = $entityManager->getRepository(Borrowing::class)->find($id);
	    if (!$borrowing) {
            throw $this->createNotFoundException(
            'No borrowing found for id '.$id
        );
        }

        $borrowing->setBorrowingStatus('Réceptionnée');
        $entityManager->flush();
        return $this->redirectToRoute('borrowing_display', [
        'id' => $borrowing->getId()
        ]);
    }

    /**
     * @Route("/borrowing/{id}/return", name="borrowing_return", methods={"GET","POST"})
     */

    public function return_borrowing(Request $request,borrowing $borrowing): Response
    {

        $form = $this->createForm(Borrowing_ReturnType::class,$borrowing);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('borrowing_display');
        }
        return $this->render('Customer/borrowing/return.html.twig', ['borrowing' => $borrowing,'form' => $form->createView()]);
    }

}
