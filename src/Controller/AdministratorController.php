<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Genres;
use App\Entity\Vinyle;
use App\Entity\Borrowing;
use App\Entity\Interpreter;
use App\Form\UserType;
use App\Form\PersoType;
use App\Form\GenresType;
use App\Form\VinyleType;
use App\Form\BorrowingType;
use App\Form\InterpreterType;
use App\Form\Borrowing_ExpeditionType;
use App\Repository\UserRepository;
use App\Repository\GenresRepository;
use App\Repository\VinyleRepository;
use App\Repository\BorrowingRepository;
use App\Repository\InterpreterRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;






/**
 * @Route("/admin")
 */
class AdministratorController extends AbstractController
{
    /*Index Adminstrateur*/

    /**
     * @Route("/", name="dashboard_index")
     */

    public function index_dashboard(){

        return $this->render('Administrator/dashboard.index.html.twig');
    }

    /* Gestion des Genres */

    /**
     * @Route("/genres", name="genres_index", methods={"GET"})
     */

    public function index_genres(GenresRepository $genresRepository): Response
    {
        return $this->render('Administrator/genres/index.html.twig', [
            'genres' => $genresRepository->findAll(),
        ]);
    }

    /**
     * @Route("/genre/new", name="genres_new", methods={"GET","POST"})
     */

    public function new_genre(Request $request): Response
    {
        $genre = new Genres();
        $form = $this->createForm(GenresType::class, $genre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($genre);
            $entityManager->flush();

            return $this->redirectToRoute('genres_index');
        }

        return $this->render('Administrator/genres/new.html.twig', [
            'genre' => $genre,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/genre/{id}", name="genres_show", methods={"GET"})
     */

    public function show_genres(Genres $genre): Response
    {
        return $this->render('Administrator/genres/show.html.twig', [
            'genre' => $genre,
        ]);
    }

    /**
     * @Route("/genre/{id}/edit", name="genres_edit", methods={"GET","POST"})
     */

    public function edit_genre(Request $request, Genres $genre): Response
    {
        $form = $this->createForm(GenresType::class, $genre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('genres_index');
        }

        return $this->render('Administrator/genres/edit.html.twig', [
            'genre' => $genre,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/genre/{id}", name="genres_delete", methods={"DELETE"})
     */

    public function delete_genre(Request $request, Genres $genre): Response
    {
        if ($this->isCsrfTokenValid('delete'.$genre->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($genre);
            $entityManager->flush();
        }

        return $this->redirectToRoute('genres_index');
    }

    /* Gestion des Interprètres */

    /**
     * @Route("/interpreter", name="interpreter_index", methods={"GET"})
     */

    public function index_interpreter(Request $request,InterpreterRepository $interpeterRepository,PaginatorInterface $paginator )
    {   
        
        $donnees = $this->getDoctrine()->getRepository(Interpreter::class)->findAll();
        $interpreters = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos articles)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            2 // Nombre de résultats par page        
        );              
          
        return $this->render('Administrator/interpreter/index.html.twig', [            
            'interpreters'=>$interpreters 
        ]);
    }

    /**
     * @Route("/interpreter/new", name="interpreter_new", methods={"GET","POST"})
     */

    public function new_interpreter(Request $request): Response
    {
        $interpreter = new Interpreter();
        $form = $this->createForm(InterpreterType::class, $interpreter);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($interpreter);
            $entityManager->flush();

            return $this->redirectToRoute('interpreter_index');
        }

        return $this->render('Administrator/interpreter/new.html.twig', [
            'interpreter' => $interpreter,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/interpreter/{id}", name="interpreter_show", methods={"GET"})
     */
    public function show_interpreter(Interpreter $interpreter): Response
    {
        return $this->render('Administrator/interpreter/show.html.twig', [
            'interpreter' => $interpreter,
        ]);
    }

    /**
     * @Route("/interpreter/{id}/edit", name="interpreter_edit", methods={"GET","POST"})
     */
    public function edit_interpreter(Request $request, Interpreter $interpreter): Response
    {
        $form = $this->createForm(InterpreterType::class, $interpreter);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('interpreter_index');
        }

        return $this->render('Administrator/interpreter/edit.html.twig', [
            'interpreter' => $interpreter,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/interpreter/{id}", name="interpreter_delete", methods={"DELETE"})
     */
    public function delete_interpreter(Request $request, Interpreter $interpreter): Response
    {
        if ($this->isCsrfTokenValid('delete'.$interpreter->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($interpreter);
            $entityManager->flush();
        }

        return $this->redirectToRoute('interpreter_index');
    }

    /* Gestions des Vinyles */

    /**
     * @Route("/vinyle", name="vinyle_index", methods={"GET"})
     */

    public function index_vinyle(Request $request, PaginatorInterface $paginator)
    {
        $donnees = $this->getDoctrine()->getRepository(Vinyle::class)->findAll();
        $vinyles = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos articles)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            5 // Nombre de résultats par page
        );   
        return $this->render('Administrator/vinyle/index.html.twig', [
            'vinyles' => $vinyles,
        ]);
    }

    /**
     * @Route("/vinyle/new", name="vinyle_new", methods={"GET","POST"})
     */

    public function new_vinyle(Request $request): Response
    {
        $vinyle = new Vinyle();
        $form = $this->createForm(VinyleType::class, $vinyle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($vinyle);
            $entityManager->flush();

            return $this->redirectToRoute('vinyle_index');
        }

        return $this->render('Administrator/vinyle/new.html.twig', [
            'vinyle' => $vinyle,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/vinyle/{id}", name="vinyle_show_administrator", methods={"GET"})
     */

    public function show_vinyle(Vinyle $vinyle): Response
    {
        return $this->render('Administrator/vinyle/show.html.twig', [
            'vinyle' => $vinyle,
        ]);
    }

    /**
     * @Route("/vinyle/{id}/edit", name="vinyle_edit_administrator", methods={"GET","POST"})
     */

    public function edit_vinyle(Request $request, Vinyle $vinyle): Response
    {
        $form = $this->createForm(VinyleType::class, $vinyle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('vinyle_index');
        }

        return $this->render('Administrator/vinyle/edit.html.twig', [
            'vinyle' => $vinyle,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/vinyle/{id}", name="vinyle_delete", methods={"DELETE"})
     */

    public function delete(Request $request, Vinyle $vinyle): Response
    {
        if ($this->isCsrfTokenValid('delete'.$vinyle->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($vinyle);
            $entityManager->flush();
        } 

        return $this->redirectToRoute('vinyle_index');
    }

    /* Gestions des Utilisateurs */

    /**
     * @Route("/user", name="user_index", methods={"GET"})
     */

    public function index_user(UserRepository $userRepository): Response
    {
        return $this->render('Administrator/user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    /**
     * @Route("/user/new", name="user_new", methods={"GET","POST"})
     */

    public function new_user(Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('Administrator/user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/user/{id}", name="user_show", methods={"GET"})
     */

    public function show_user(User $user): Response
    {
        return $this->render('Administrator/user/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/user/{id}/edit", name="user_edit", methods={"GET","POST"})
     */

    public function edit_user(Request $request, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('Administrator/user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/user/{id}", name="user_delete", methods={"DELETE"})
     */

    public function delete_user(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_index');
    }
    
    /* Gestions des commandes */

    /**
     * @Route("/borrowing", name="borrowing_index", methods={"GET"})
     */

    public function index_borrowing(BorrowingRepository $borrowingRepository): Response
    {
        return $this->render('Administrator/borrowing/index.html.twig', [
            'borrowings' => $borrowingRepository->findAll(),
        ]);
    }

    /**
     * @Route("/borrowing/new", name="borrowing_new", methods={"GET","POST"})
     */

    public function new_borrowing(Request $request): Response
    {
        $borrowing = new Borrowing();
        $form = $this->createForm(BorrowingType::class, $borrowing);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($borrowing);
            $entityManager->flush();

            return $this->redirectToRoute('borrowing_index');
        }

        return $this->render('Administrator/borrowing/new.html.twig', [
            'borrowing' => $borrowing,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/borrowing/{id}", name="borrowing_show", methods={"GET"})
     */

    public function show_borrowing(Borrowing $borrowing): Response
    {
        return $this->render('Administrator/borrowing/show.html.twig', [
            'borrowing' => $borrowing,
        ]);
    }

    /**
     * @Route("/borrowing/{id}/edit", name="borrowing_edit", methods={"GET","POST"})
     */

    public function edit_borrowing(Request $request, Borrowing $borrowing): Response
    {
        $form = $this->createForm(BorrowingType::class, $borrowing);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('borrowing_index');
        }

        return $this->render('Administrator/borrowing/edit.html.twig', [
            'borrowing' => $borrowing,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/borrowing/{id}", name="borrowing_delete", methods={"DELETE"})
     */

    public function delete_borrowing(Request $request, Borrowing $borrowing): Response
    {
        if ($this->isCsrfTokenValid('delete'.$borrowing->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($borrowing);
            $entityManager->flush();
        }

        return $this->redirectToRoute('borrowing_index');
    }
     /* Gestions des abonnements */

    /**
     * @Route("/subcription", name="subscription_index", methods={"GET"})
     */

    public function index_subscription(UserRepository $userRepository): Response
    {
        return $this->render('Administrator/subscription management/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }
   
    /**
     * @Route("/user/subscription/edit/{id}/{status}", name="subscription_validation", methods={"GET","POST"})
     */

    public function validate($id,$status)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(User::class)->find($id);
        if (!$user) {
            throw $this->createNotFoundException(
                'No user found for id '.$id
        );
        }
        $user->setSubscriptionStatus($status);
        $entityManager->flush();
        return $this->redirectToRoute('subscription_index', [
            'id' => $user->getId()
        ]);
    }

    /**
     * @Route("/user/{id}/perso", name="perso_edit", methods={"GET","POST"})
     */

    public function perso_edit(Request $request,user $user): Response
    {

        $form = $this->createForm(PersoType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('user_index');
        }
        return $this->render('Customer/perso/edit.html.twig', ['user' => $user,'form' => $form->createView(),]);
    }
    /**
     * @Route("/borrowing/{id}/expedition", name="borrowing_expedition", methods={"GET","POST"})
     */

    public function expedition_borrowing(Request $request,borrowing $borrowing): Response
    {

        $form = $this->createForm(Borrowing_ExpeditionType::class,$borrowing);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('borrowing_index');
        }
        return $this->render('Administrator/borrowing/expedition.html.twig', ['borrowing' => $borrowing,'form' => $form->createView()]);
    }

    /**
     * @Route("/borrowing/{id}/{ref}/closure", name="borrowing_closure", methods={"GET","POST"})
     */

    public function close_borrowing($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $borrowing = $entityManager->getRepository(Borrowing::class)->find($id);
	    if (!$borrowing) {
            throw $this->createNotFoundException(
            'No borrowing found for id '.$id
        );
        }
        $borrowing->setBorrowingStatus('Cloturée');
        $entityManager->flush();


        return $this->redirectToRoute('borrowing_index', ['id' => $borrowing->getId()]);
    }

    /**
     * @Route("/borrowing/{ref}/{id}/empty", name="borrowing_empty", methods={"GET","POST"})
     */

    public function empty_borrowing($ref,$id,VinyleRepository $vinyleRepository)
    {
        $result=$vinyleRepository->clear_borrowing($ref);
        return $this->close_borrowing($id);
        return $this->redirectToRoute('borrowing_index');    
    
    }

}


