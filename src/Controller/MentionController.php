<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MentionController extends AbstractController
{
    /**
     * @Route("/mention", name="mention")
     */
    
    public function index()
    {
        return $this->render('Common/mention/index.html.twig', [
            'controller_name' => 'MentionController',
        ]);
    }
}
