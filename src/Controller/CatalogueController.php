<?php

namespace App\Controller;

use DateTime;
use App\Entity\User;
use App\Entity\Vinyle;
use App\Entity\Borrowing;
use App\Form\CatalogType;
use App\Form\InterpreterType;
use App\Form\SearchVinyleType;
use App\Form\VinyleSearchType;
use Doctrine\ORM\EntityManager;
use App\Repository\VinyleRepository;
use App\Repository\BorrowingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
    * @Route("/catalogue")
    */
class CatalogueController extends AbstractController
{
    /**
     * @Route("/", name="catalogue")
     */
    public function index(Request $request,VinyleRepository $vinyleRepository, PaginatorInterface $paginator)
    {

        $donnees = $this->getDoctrine()->getRepository(Vinyle::class)->findAll();
        $vinyle = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos articles)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            5 // Nombre de résultats par page
        );
        $vinyles=[];
        $form = $this->createForm(SearchVinyleType::class);

        $search=$form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            // ici on recherche les vinyles correspondant aux mots clés
            $vinyles= $vinyleRepository->search(
                $search->get('mots')->getData(),
                $search->get('interpreter')->getData(),
                $search->get('vinyle_genre')->getData());

           /* dd($vinyles);*/

        }
        return $this->render('Common/Catalogue/index.html.twig', [
            'vinyles' => $vinyles,
            'form'=>$form->createView(),
             'vinyle'=>$vinyle
        ]);
    }
    /**
     * @Route("/new", name="vinyle_new_customer", methods={"GET","POST"})
     */

    public function new(Request $request): Response
    {
        $vinyle = new Vinyle();
        $form = $this->createForm(VinyleType::class, $vinyle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($vinyle);
            $entityManager->flush();

            return $this->redirectToRoute('catalogue');
        }

        return $this->render('Common/Catalogue/new.html.twig', [
            'vinyle' => $vinyle,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}", name="vinyle_show", methods={"GET"})
     */

    public function show(Vinyle $vinyle): Response
    {
        return $this->render('Common/Catalogue/show.html.twig', [
            'vinyle' => $vinyle,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="vinyle_edit", methods={"GET","POST"})
     */

    public function edit(Request $request, Vinyle $vinyle): Response
    {
        $form = $this->createForm(VinyleType::class, $vinyle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('catalogue');
        }

        return $this->render('Common/Catalogue/edit.html.twig', [
            'vinyle' => $vinyle,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="vinyle_delete", methods={"DELETE"})
     */

    public function delete(Request $request, Vinyle $vinyle): Response
    {
        if ($this->isCsrfTokenValid('delete'.$vinyle->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($vinyle);
            $entityManager->flush();
        }

        return $this->redirectToRoute('catalogue');
    }

    /**
     * @Route("/{interpreter}/search",name="search_interpreter", methods={"GET","POST"} )
     */

    public function findbyinterpreter($interpreter,VinyleRepository $vinyleRepository)
    {
        $result= $vinyleRepository->findByInterpreter($interpreter);
        return $this->redirectToRoute('catalogue');
    }
}
