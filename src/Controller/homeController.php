<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class homeController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     */
    
    public function index()
    {
        if($this->getUser())
        {
            $userrole = $this->getUser()->getRoles(); 
            $nbroles=count($userrole);
            if($nbroles>=1)
            {
                if ($userrole[0]=='ROLE_ADMIN')
                {
                    $route='dashboard_index';
                }
               
            }
                    if ($userrole[0]=='ROLE_USER')
                    {
                        $route='dashboard_index_customer';
                    }; 
                    return $this->redirectToRoute($route);
        }
        return $this->render('Common/home/home.html.twig', [
            'controller_name' => 'Home2Controller',
        ]);
    }
}
